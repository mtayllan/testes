#include <stdio.h>
#include <string.h>

void intersec(char *str1, char *str2, char *resp){
	int i,j; int k=0;
	for(i=0; str1[i] != '\0' && str1[i] != '\n'; i++){
        for(j=0; str2[j] != '\0' && str2[j] != '\n'; j++){
            if(str1[i]==str2[j]){
                resp[k] = str1[i];
                k++;
                break;
            }
        }
    }
    resp[k] = '\0';
}

int main(){
    char g1[] = "ABE";
    char g2[] = "ABCD";
    char resp[20]; resp[0] = '\0'; 
    intersec(g1,g2,resp);
    printf("%s", resp);
}