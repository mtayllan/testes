#include <stdio.h>
#include <string.h>

void sum(char *str1, char *str2);
void dif(char *str1, char *str2);
void intersec(char *str1, char *str2);
int isOperation(char op);
void execute(char *str1, char *str2, char op);
int hasChar(char *str, char c);

char resp[20];

int main(void) {
	char groups[10][10];
	int k=0; //kuantidade /prencher ultimo com \0

	char text[100];
	fgets(text,100,stdin);

	//separa em grupos
	int i,j;
	for(i=0; text[i] != '\n' ; i++){
		if(text[i]=='{'){
			int j=0;
			while(text[i]!='}'){
				groups[k][j] = text[i+1];
				j++; i++;
			}
			groups[k][j-1] = '\0';
			k++;
		}
	}
	
	//cria notação
	char not[20]; int t = 0; int gc = 0;
	
	for( i = 0; text[i] != '\0' && text[i] != '\n'; i++){
		if(text[i] == '{'){
			while(text[i]!='}') i++;
			not[t++] = (gc++) + 48;
		}else{
			not[t++] = text[i]; 
		}
	}

	not[t] = '\0';

	//resposta	
	int execs = 0; //numero de execuções já feitas
	if(strlen(not) < 3){
		for(i=0; groups[0][i] != '\0' && groups[0][i] != '\n'; i++) resp[i] = groups[0][i];
	}else if(hasChar(not, '*') && !hasChar(not, '(') ){
		for(i=0; not[i]!='*'; i++);
		int ig1 = not[i-1] - 48;
		int ig2 = not[i+1] - 48;
		char *group1 = groups[ig1];
		char *group2 = groups[ig2]; 
		execute(group1, group2, not[i]);
		not[i-1] = not[0];
		not[i] = '\0';
		t-=2;
		for( i = 0; i < t; i++)
		{
			if(isOperation(not[i])){
					int ig = not[i+1] - 48;
					char *group1 = resp;
					char *group2 = groups[ig];
					execute(group1, group2,  not[i]);
			}
		}
	}else{
		for( i = 0; i < t; i++)
		{
			if(not[i]=='('){
				while(not[i] != ')'){
					if(isOperation(not[i])){
						int ig1 = not[i-1] - 48;
						int ig2 = not[i+1] - 48;
						char *group1 = groups[ig1];
						char *group2 = groups[ig2]; 
						execute(group1, group2, not[i]);
						execs++;
					}
					i++;
				}
			}else if(isOperation(not[i])){
				if(execs==0){
					int ig1 = not[i-1] - 48;
					int ig2 = not[i+1] - 48;
					char *group1 = groups[ig1];
					char *group2 = groups[ig2];
					execute(group1, group2,  not[i]);
					execs++;
				}else{
					int ig = not[i+1] - 48;
					char *group1 = resp;
					char *group2 = groups[ig];
					execute(group1, group2,  not[i]);
				}
			}
		}
	}

	int aux;
    for (k = strlen(resp) - 1; k > 0; k--) {
        for (j = 0; j < k; j++) {
            if (resp[j] > resp[j + 1]) {
                aux          = resp[j];
                resp[j]     = resp[j + 1];
                resp[j + 1] = aux;
            }
        }
    }

	printf("{%s}", resp);
	return 0;
}

int hasChar(char *str, char c){
	int i;
	for(i=0; str[i]!= '\0'  && str[i] != '\n'; i++){
		if(str[i] == c) return 1;
	}
	return 0;
}

int isOperation(char op){
	int i;
	char opers[3] = "+-*";
	for(i=0; i<3; i++){
		if(opers[i]==op){
			return 1;
		}
	}
	return 0;
}

void execute(char *str1, char *str2, char op){
	if(op == '*'){
		intersec(str1, str2);
	}else if(op == '+'){
		sum(str1,str2);
	}else if(op == '-'){
		dif(str1,str2);
	}
}

void sum(char *str1, char *str2){
	int i;
	for(i=0; str1[i]!='\0' && str1[i]!='\n'; i++){
		resp[i] = str1[i];
	}
	int tam = i;

	for(i=0; str2[i]!='\0' && str2[i]!='\n'; i++){
		int esta = 0;
		int j;
		for(j=0; j<tam; j++){
			if(resp[j]==str2[i]){
				esta = 1; break;
			}
		}
		if(!esta){
			resp[tam] = str2[i];
			tam++;
		}			
	}
	resp[tam] = '\0';	
}

void dif(char *str1, char *str2){
	int i;
	char subs[10]; int k=0; int m =0;
	for(i=0; str2[i]!='\0' && str2[i]!='\n'; i++){
		int j;
		for(j=0; str1[j]!='\0' && str1[j]!='\n'; j++){
			if(str1[j] == str2[i]){
				subs[k] = str1[j];
				k++;
			}
		}
	}

	for(i=0; str1[i]!='\0' && str1[i]!='\n'; i++){
		int j; int esta=0;
		for(j=0; j<k; j++){
			if(subs[j] == str1[i]){
				esta = 1; break;
			}
		}
		if(!esta){
			resp[m] = str1[i];
			m++;
		}
	}
	resp[m] = '\0';
}

void intersec(char *str1, char *str2){
	int i,j; int k=0;
	for(i=0; str1[i] != '\0' && str1[i] != '\n'; i++){
        for(j=0; str2[j] != '\0' && str2[j] != '\n'; j++){
            if(str1[i]==str2[j]){
                resp[k] = str1[i];
                k++;
                break;
            }
        }
    }
    resp[k] = '\0';
}