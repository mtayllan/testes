#include <stdio.h>
#include <string.h>

void dif(char *str1, char *str2, char *resp){
	int i;
	char subs[10]; int k=0; int m =0;
	for(i=0; str2[i]!='\0' && str2[i]!='\n'; i++){
		int j;
		for(j=0; str1[j]!='\0' && str1[j]!='\n'; j++){
			if(str1[j] == str2[i]){
				subs[k] = str1[j];
				k++;
			}
		}
	}

	for(i=0; str1[i]!='\0' && str1[i]!='\n'; i++){
		int j; int esta=0;
		for(j=0; j<k; j++){
			if(subs[j] == str1[i]){
				esta = 1; break;
			}
		}
		if(!esta){
			resp[m] = str1[i];
			m++;
		}
	}
	resp[m] = '\0';

}

int main(){
    char g1[] = "ABC";
    char g2[] = "CDE";
    char resp[20]; resp[0] = '\0'; 
    dif(g1,g2,resp);
    printf("%s", resp);
}