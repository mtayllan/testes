
Você quer planejar uma grande festa de aniversário com seus amigos. Durante o planejamento você percebeu que você deve fazer inúmeros operações com conjuntos de amigos. Existe um grupo que consiste do Arthur, Biene e Clemens. Existe outro grupo de amigos que você conhece do snowboarding que consiste do Daniel, Ernst, Frida e Gustav. Se você quer convidar ambos, o resultado do grupo da festa consiste de g1 + g2 (o resultado é a união de ambos os grupos). Então você pode computar a intersecção dos dois grupos g1 * g2, que consiste no conjunto vazio. Talvez você queira convidar o grupo g1, mas excluindo todos os membros do outro grupo g2, que pode ser escrito como g1 - g2. Intersecção (*) precede sobre união (+) e diferença (-). Todas as operações são associadas a esquerda, o que significa que em A op 1 B op 2 C você primeiro deve avaliar A op 1 B (desde que op 1 e op 2 possuam uma precedência igual).

### Entrada e Saida

Entrada:

-   Uma string contendo a expressão a ser avaliada.

Saida:

-   Uma string contendo o resultado da expressão em ordem alfabética.

### Exemplos

```
>> {ABC}
<< {ABC}
```

```
>> {ABC}+{DEFG}+{Z}+{}
<< {ABCDEFGZ}
```

```
>> {ABE}*{ABCD}
<< {AB}
```

```
>> {ABCD}-{CZ}
<< {ABD}
```

```
>> {ABC}+{CDE}*{CEZ}
<< {ABCE}
```

```
>> ({ABC}+{CDE})*{CEZ}
<< {CE}
```