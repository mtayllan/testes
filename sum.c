#include <stdio.h>
#include <string.h>

void sum(char *str1, char *str2, char *resp){
	int i;
	for(i=0; str1[i]!='\0' && str1[i]!='\n'; i++){
		resp[i] = str1[i];
	}
	int tam = i;

	for(i=0; str2[i]!='\0' && str2[i]!='\n'; i++){
		int esta = 0;
		int j;
		for(j=0; j<tam; j++){
			if(resp[j]==str2[i]){
				esta = 1; break;
			}
		}
		if(!esta){
			resp[tam] = str2[i];
			tam++;
		}			
	}
	resp[tam] = '\0';	
}

int main(){
    char g1[] = "ABC";
    char g2[] = "CDE";
    char resp[20]; resp[0] = '\0'; 
    sum(g1,g2,resp);
    printf("%s", resp);
}

